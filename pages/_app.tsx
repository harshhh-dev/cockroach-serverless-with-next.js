import '../styles/globals.css'
import type { AppProps } from 'next/app'

function CockroachPrismaDemo({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}

export default CockroachPrismaDemo
