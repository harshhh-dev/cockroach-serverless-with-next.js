import type { NextApiRequest, NextApiResponse } from 'next'

import prisma from '../../lib/prisma'

const newTrack = async (req: NextApiRequest, res: NextApiResponse) => {
  const { title, content, link } = req.body

  try {
    const post = await prisma.post.create({
      data: {
        title: title,
        content: content,
        link: link,
      },
    })

    const slug = post.id.toString().substring(0, post.id.toString().length)
    return res.status(200).json({ success: true, slug: slug })
  } catch (err) {
    return res.status(509).json({ error: err })
  }
}

export default newTrack
