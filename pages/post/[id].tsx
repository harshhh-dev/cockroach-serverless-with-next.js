import type { GetServerSideProps, NextPage } from 'next'

import styles from '../../styles/Home.module.css'

import SEO from '../../components/SEO'
import Nav from '../../components/Nav'
import View from '../../components/View'

import prisma from '../../lib/prisma'
import { post as PostType } from '@prisma/client'

const PostView: NextPage<{ post: PostType }> = ({ post }) => {
  return (
    <div className={styles.container}>
      <SEO />
      <div className={styles.box}>
        <Nav />
        <View post={post} />
      </div>
    </div>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const post = await prisma.post.findUnique({
    where: {
      id: BigInt(context.params!.id!.toString()),
    },
  })

  return {
    props: {
      post,
    },
  }
}

export default PostView
