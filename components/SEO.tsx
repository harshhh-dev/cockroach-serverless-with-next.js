import { FC } from 'react'
import Head from 'next/head'

const SEO: FC = () => {
  return (
    <Head>
      <title>Music Lovers</title>
      <meta
        name='description'
        content='A sample app for migrating over an existing PostgreSQL database into CockroachDB'
      />
      <link
        rel='icon'
        href='data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>🎷</text></svg>'
      />
    </Head>
  )
}

export default SEO
