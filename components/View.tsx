import { FC } from 'react'
import styles from '../styles/View.module.css'
import { post as PostType } from '@prisma/client'

const View: FC<{ post: PostType }> = ({ post }) => {
  return (
    <div className={styles.post}>
      <h1>{post.title}</h1>
      <div className={styles.info}>
        <a
          href={post.link}
          target='_blank'
          rel='noreferrer'
          className={styles.link}
        >
          {post.link}
        </a>
        <p>Written on {post.createdat.toLocaleDateString()}</p>
      </div>
      <p>{post.content}</p>
    </div>
  )
}

export default View
